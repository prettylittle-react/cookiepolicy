import React from 'react';
import PropTypes from 'prop-types';

import Container from 'container';
import ContentBlock from 'contentblock';
import ContentExpander from 'contentexpander';

import Cookie from 'cookie';

import './CookiePolicy.scss';

const COOKIE_NAME = 'cookiePolicyAccepted';

/**
 * CookiePolicy
 * @description [Description]
 * @example
  <div id="CookiePolicy"></div>
  <script>
    ReactDOM.render(React.createElement(Components.CookiePolicy, {
    	title : 'Example CookiePolicy'
    }), document.getElementById("CookiePolicy"));
  </script>
 */
class CookiePolicy extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'cookiepolicy';

		this.state = {
			isOpen: false,
			accepted: SERVER
				? false
				: String(Cookie.get(`${props.cookiePrefix}${COOKIE_NAME}`)) === String(props.lastUpdated)
		};
	}

	componentDidMount() {
		const {delay} = this.props;

		if (!this.component) {
			return;
		}

		setTimeout(() => {
			this.setState({isOpen: true});
		}, delay);
	}

	onClick = e => {
		e.preventDefault();

		const {lastUpdated, cookiePrefix, expirationDays} = this.props;

		Cookie.set(`${cookiePrefix}${COOKIE_NAME}`, lastUpdated, expirationDays);

		this.setState({isOpen: false});
	};

	render() {
		const {acceptText} = this.props;
		const {isOpen, accepted} = this.state;

		if (accepted) {
			return null;
		}

		const cta = [
			{
				type: 'button',
				onClick: this.onClick,
				label: acceptText,
				style: 'primary'
			}
		];

		return (
			<ContentExpander
				className={`${this.baseClass}`}
				isOpen={isOpen}
				ref={component => (this.component = component)}
			>
				<div className={`${this.baseClass}__main`}>
					<Container>
						<ContentBlock {...this.props} titleSize={3} cta={cta} />
					</Container>
				</div>
			</ContentExpander>
		);
	}
}

CookiePolicy.defaultProps = {
	delay: 1000,
	acceptText: 'Accept Cookies',
	title: '',
	content: '',
	lastUpdated: 'never',
	cookiePrefix: '',
	expirationDays: 365
};

CookiePolicy.propTypes = {
	delay: PropTypes.number.isRequired,
	acceptText: PropTypes.string.isRequired,
	title: PropTypes.string,
	content: PropTypes.string.isRequired,
	lastUpdated: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
	cookiePrefix: PropTypes.string,
	expirationDays: PropTypes.number
};

export default CookiePolicy;
